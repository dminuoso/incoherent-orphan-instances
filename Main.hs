module Main where

import Data.Set
import A
import B
import T


main :: IO ()
main = do
  print colors
  print (addCyan colors)
  print (addPurple (addCyan colors))
