module T where

newtype T = T String
  deriving Show
