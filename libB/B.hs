module B where

import T
import Data.Set

instance Eq T where
  _ == _ = True

instance Ord T where
  compare _ _ = EQ

addPurple :: Set T -> Set T
addPurple = insert (T "purple")
