module A where

import T
import Data.Set

instance Eq T where
  T l == T r = l == r

instance Ord T where
  compare (T l) (T r) = compare l r

addCyan :: Set T -> Set T
addCyan = insert (T "cyan")

colors :: Set T
colors = fromList [T "red", T "blue", T "green"]
